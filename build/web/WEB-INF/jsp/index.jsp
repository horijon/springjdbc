<%@include file="header.jsp" %>
<table class="table table-bordered table-condensed table-hover table-responsive table-striped">
    <tr>
        <th>Id</th>
        <th>Name</th>
    </tr>
    <c:forEach var="department" items="${departments}">
        <tr>
            <td>${department.id}</td>
            <td>${department.name}</td>
        </tr>
    </c:forEach>
</table>
<h1>${department.name}</h1>
<%@include file="footer.jsp" %>