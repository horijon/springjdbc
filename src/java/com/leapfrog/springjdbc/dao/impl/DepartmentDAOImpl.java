/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.leapfrog.springjdbc.dao.impl;

import com.leapfrog.springjdbc.dao.DepartmentDAO;
import com.leapfrog.springjdbc.entity.Department;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.stereotype.Repository;

/**
 *
 * @author kshitij
 */
@Repository(value = "departmentDAO")
public class DepartmentDAOImpl implements DepartmentDAO{

    @Autowired
    private JdbcTemplate jdbcTemplate;
    
    @Override
    public int insert(Department d) throws SQLException {
        String sql = "INSERT INTO tbl_departments(name) VALUES(?)";
        return jdbcTemplate.update(sql,new Object[]{d.getName()});
    }

    @Override
    public int update(Department d) throws SQLException {
        String sql = "UPDATE tbl_departments SET name=? WHERE id=?";
        return jdbcTemplate.update(sql,new Object[]{d.getName(), d.getId()});
    }

    @Override
    public int delete(int id) throws SQLException {
        String sql = "DELETE FROM tbl_departments WHERE id=?";
        return jdbcTemplate.update(sql,new Object[]{id});
    }

    @Override
    public List<Department> getAll() throws SQLException {
        String sql = "SELECT * FROM tbl_departments";
        return jdbcTemplate.query(sql, new RowMapper<Department>() {

            @Override
            public Department mapRow(ResultSet rs, int i) throws SQLException {
                Department dept = new Department();
                dept.setId(rs.getInt("id"));
                dept.setName(rs.getString("name"));
                return dept;
            }
        });
    }

    @Override
    public Department getById(int id) throws SQLException {
        String sql = "SELECT * FROM tbl_departments WHERE id=?";
        return jdbcTemplate.queryForObject(sql, new Object[]{id}, new RowMapper<Department>() {

            @Override
            public Department mapRow(ResultSet rs, int i) throws SQLException {
                Department d = new Department();
                d.setId(rs.getInt("id"));
                d.setName(rs.getString("name"));
                return d;
            }
        });
    }
}
