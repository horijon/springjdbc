/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.leapfrog.springjdbc.dao;

import com.leapfrog.springjdbc.entity.Department;
import java.sql.SQLException;
import java.util.List;

/**
 *
 * @author kshitij
 */
public interface DepartmentDAO {
    int insert(Department d) throws SQLException;
    int update(Department d) throws SQLException;
    int delete(int id) throws SQLException;
    List<Department> getAll() throws SQLException;
    Department getById(int id) throws SQLException;
}
