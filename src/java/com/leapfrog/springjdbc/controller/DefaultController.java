/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.leapfrog.springjdbc.controller;

import com.leapfrog.springjdbc.dao.DepartmentDAO;
import com.leapfrog.springjdbc.entity.Department;
import java.sql.SQLException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;

/**
 *
 * @author kshitij
 */
@Controller
@RequestMapping(value = "/")
public class DefaultController {
    
    @Autowired
    private DepartmentDAO departmentDAO;
    
    @RequestMapping(method = RequestMethod.GET)
    public ModelAndView index(){
        ModelAndView mv = new ModelAndView("index");
        try{
            mv.addObject("departments", departmentDAO.getAll());
            mv.addObject("department", departmentDAO.getById(5));
        }catch(SQLException s){
            s.getMessage();
        }
        
        return mv;
    }
}
