<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<c:set var="SITE_URL" value="${pageContext.request.contextPath}"/>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>Spring JDBC Project</title>
        <link rel="icon" type="image/gif" href="${SITE_URL}/bootstrap/favicon/favicon_flower.ico"/>
        <script src="${SITE_URL}/bootstrap/assets/js/jquery-1.12.0.min.js"></script>
        <link rel="stylesheet" type="text/css" href="${SITE_URL}/bootstrap/assets/css/bootstrap-theme.min.css"/>
        <link rel="stylesheet" type="text/css" href="${SITE_URL}/bootstrap/assets/css/bootstrap.min.css"/>
        <script src="${SITE_URL}/bootstrap/assets/js/bootstrap.min.js"></script>
    </head>
    <body>
        <div class="container">
